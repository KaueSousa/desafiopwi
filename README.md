# README #

Esse projeto possui a realização do desafio "List-To-Do", sendo um ASP.NET MVC desenvolvido em C#.

### Início ###
Para executá-lo será necessário instalar os seguintes programas:
* Microsoft Visual Studio 2017 ou superior;
* Nuget: "MySqlData" na versão mais recente.

Além disso, é necessário desinstalar o seguinte Nuget para que não haja conflitos:
* Microsoft.Code.Dom.Providers.DotNetCompilerPlatform

Por fim, é importante que uma(s) alteração(ões) seja(m) realizada(s) no comando de conexão com o banco de dados
(Database.ItemDatabase(MySqlConnection)), caso surgir algum problema na execução.

### Desenvolvimento ###

* Para acessar o projeto é necessário clonar o seguinte link do GitHub:
git clone https://KaueSousa@bitbucket.org/KaueSousa/desafiopwi.git


### Deploy e Publicação ###

* Para acessar o projeto de forma on-line é preciso clicar no seguinte link:
http://kauejesussousa-001-site1.ftempurl.com/

### Features ###

O projeto tem como objetivo montar uma lista de nomes/atividades que deseja adicionar, realizando um CRUD
(Create, Read, Update e Delete) a partir do item desejado.
* Apertando em "Novo Item" é possível adicionar um item a lista;
* A partir da exibição dos itens na lista, duas ações surgem: "Editar" e "Remover", possibilitando o clique 
em cada uma delas para realizar suas devidas funções.

### Configuração ###

* Para executar o projeto pode ser necessário a mudança dos dados de conexão com o banco e, ao mesmo tempo, 
seu próprio script.

### Contribuições ###

* Contribuições são bem-vindas! Caso deseje contribuir aplicando melhorias lembre-se de manter a codificação em C#. 

### Licença ###

Licença MIT, todos os detalhes estão presentes no arquivo "LICENSE.txt"