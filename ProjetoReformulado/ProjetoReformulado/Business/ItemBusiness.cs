﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoReformulado.Business
{
    public class ItemBusiness
    {
        // Instaciando o Database
        Database.ItemDatabase itemdb = new Database.ItemDatabase();

        public void InserirItem(Models.ItemModel item)
        {
            // Recebendo o resultado da busca do item
            bool contem = itemdb.BuscarItem(item.Nome);
            
            // Verifica se o nome não está nulo ou vazio e utiliza a função de busca
            if(!string.IsNullOrEmpty(item.Nome) && contem == false)
              itemdb.InserirItem(item);
        }

        // Recebe a lista do database
        public List<Models.ItemModel> _ListarTodos()
        {
            List<Models.ItemModel> lista = itemdb._ListarTodos();
            return lista;
        }

        // Recebe o modelo do database
        public Models.ItemModel FiltrarPorId(int id)
        {
            Models.ItemModel model = itemdb.FiltrarPorId(id);
            return model;
        }

        // Recebe a função de alterar do database
        public void AlterarItem(Models.ItemModel item)
        {
            bool contem = itemdb.BuscarItem(item.Nome);

            if (!string.IsNullOrEmpty(item.Nome) && contem == false)
                itemdb.AlterarItem(item);
        }

        // Recebe a função de remover do database
        public void RemoverItem(int id)
        {
            if(id > 0)
            itemdb.RemoverItem(id);
        }
    }
}