﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjetoReformulado.Models
{
    public class ItemModel
    {
        public int Id { get; set; }
        [MaxLength(100, ErrorMessage = "O nome do item não pode conter mais de 100 caracteres")]
        [DisplayName("Nome do Item")]
        [Required(ErrorMessage = "Informe um nome")]
        [RegularExpression(@"^[aA-zZ]+((\s[aA-zZ]+)+)?$", ErrorMessage = "O nome do item não pode conter números")]
        public string Nome { get; set; }
    }
}