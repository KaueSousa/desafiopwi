﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoReformulado.Database
{
    public class ItemDatabase
    {
        // Cria a conexão com o banco de dados e passa os valores para realiza-la        
        MySqlConnection cnc = new MySqlConnection("server=mysql5045.site4now.net;database=db_a7949d_desafio;uid=a7949d_desafio;pwd=1234");

        public void InserirItem(Models.ItemModel item)
        {
            // Abre a conexão
            cnc.Open();

            // Cria o comando que será realizado e define qual é o script
            MySqlCommand command = cnc.CreateCommand();
            command.CommandText = "insert into tb_item(nm_item) values (@nm_item)";

            // Passa o parâmetro que será inserido no banco de dados
            command.Parameters.Add(new MySqlParameter("nm_item", item.Nome));

            // Executa o comando
            command.ExecuteNonQuery();

            // Fecha a conexão com o banco
            cnc.Close();
        }

        public List<Models.ItemModel> _ListarTodos()
        {
            cnc.Open();

            MySqlCommand command = cnc.CreateCommand();
            command.CommandText = "select * from tb_item";

            // Executa o comando para ler os dados cadatrados
            MySqlDataReader reader = command.ExecuteReader();

            // Cria uma lista para receber os dados
            List<Models.ItemModel> lista = new List<Models.ItemModel>();

            // Condição que permanece em execução enquanto houverem dados para ler
            while(reader.Read())
            {
                // Instancia um modelo e transfere os dados encontrados no banco para carrega-lo 
                Models.ItemModel model = new Models.ItemModel();
                model.Id = reader.GetInt32("id_item");
                model.Nome = Convert.ToString(reader["nm_item"]);

                // Adiciona o(s) modelo(s) formado(s) para a lista
                lista.Add(model);
            }
            cnc.Close();

            return lista;
        }

        public Models.ItemModel FiltrarPorId(int id)
        {
            cnc.Open();

            MySqlCommand command = cnc.CreateCommand();
            command.CommandText = "select * from tb_item where id_item = @id_item";
            command.Parameters.Add(new MySqlParameter("id_item", id));

            MySqlDataReader reader = command.ExecuteReader();

            Models.ItemModel model = null;

            if (reader.Read())
            {
                model = new Models.ItemModel();
                model.Id = reader.GetInt32("id_item");
                model.Nome = Convert.ToString(reader["nm_item"]);
            }
            reader.Close();
            cnc.Close();

            return model;
        }

        public bool BuscarItem(string item)
        {
            cnc.Open();

            MySqlCommand command = cnc.CreateCommand();
            command.CommandText = "select * from tb_item where nm_item = @nm_item";
            command.Parameters.Add(new MySqlParameter("nm_item", item));

            MySqlDataReader reader = command.ExecuteReader();

            Models.ItemModel model = null;

            // Condição que avalia se já existe um item com o mesmo nome cadastrado
            if (reader.Read())
            {
                model = new Models.ItemModel();
                model.Id = reader.GetInt32("id_item");
                model.Nome = Convert.ToString(reader["nm_item"]);

                reader.Close();
                cnc.Close();
                return true;
            }
            else
            {
                cnc.Close();
                return false;
            }                   
        }

        public void AlterarItem(Models.ItemModel item)
        {
            cnc.Open();

            MySqlCommand command = cnc.CreateCommand();
            command.CommandText = @"update tb_item set nm_item = @nm_item 
                                                        where id_item = @id_item";
       
            command.Parameters.Add(new MySqlParameter("id_item", item.Id));
            command.Parameters.Add(new MySqlParameter("nm_item", item.Nome));
            command.ExecuteNonQuery();
            cnc.Close();
        }

        public void RemoverItem(int id)
        {
            cnc.Open();

            MySqlCommand command = cnc.CreateCommand();
            command.CommandText = "delete from tb_item where id_item = @id_item";

            command.Parameters.Add(new MySqlParameter("id_item", id));
            command.ExecuteNonQuery();
            cnc.Close();
        }
    }
}