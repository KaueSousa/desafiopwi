﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjetoReformulado.Controllers
{
    public class ItemController : Controller
    {
        Business.ItemBusiness business = new Business.ItemBusiness();
        string resultado = "OK";
        List<string> mensagem = new List<string>();

        // Carrega a exibição inicial
        public ActionResult Início()
        {
            if (!ModelState.IsValid)
            {
                resultado = "AVISO";
                mensagem = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            }
            else
            {
                try
                {
                    List<Models.ItemModel> lista = business._ListarTodos();
                    return View(lista);
                }
                catch (Exception)
                {
                    resultado = "Não foi possível iniciar o programa";
                }
            }

            return Json(new { Resultado = resultado, Mensagem = mensagem });
        }

        // Carrega a exibição de inserir um item
        public ActionResult InserirItem()
        {
            if (!ModelState.IsValid)
            {
                resultado = "AVISO";
                mensagem = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            }
            else
            {
                try
                {
                    return View();
                }
                catch (Exception)
                {

                    resultado = "Não foi possível acessar essa tela";
                }
            }

            return Json(new { Resultado = resultado, Mensagem = mensagem });
        }

        // Executa a ação de inserir quando o botão é clicado
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InserirItem(Models.ItemModel item)
        {
            // ModelState.AddModelError("", "Item inválido.");
              if (!ModelState.IsValid)
              {
                resultado = "AVISO";
                mensagem = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
              }
              else
              {
                try
                {
                    business.InserirItem(item);
                    return RedirectToAction("Início");

                }
                catch (Exception)
                {
                    resultado = "Não foi possível inserir esse item";
                    
                }
              }
            return View();
            //return Json(new { Resultado = resultado, Mensagem = mensagem });                 
        }

        // Carrega a exibição dos itens 
        public ActionResult _ListarTodos()
        {
            if (!ModelState.IsValid)
            {
                resultado = "AVISO";
                mensagem = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            }
            else
            {
                try
                {
                    List<Models.ItemModel> lista = business._ListarTodos();
                    return View(lista);
                }
                catch (Exception)
                {
                    resultado = "Não foi possível listar os itens";
                }
            }

            return Json(new { Resultado = resultado, Mensagem = mensagem });
        }

        // Carrega a exibição de alterar um item
        public ActionResult AlterarItem(int id)
        {
            if (!ModelState.IsValid)
            {
                resultado = "AVISO";
                mensagem = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            }
            else
            {
                try
                {
                    Models.ItemModel model = business.FiltrarPorId(id);
                    return View(model);
                }
                catch (Exception)
                {
                    resultado = "Não foi possível acessar essa opção";
                }
            }

            return Json(new { Resultado = resultado, Mensagem = mensagem });
        }

        // Executa a ação de alterar quando o botão é clicado
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarItem(Models.ItemModel item)
        {
            if (!ModelState.IsValid)
            {
                resultado = "AVISO";
                mensagem = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            }
            else
            {
                try
                {
                    business.AlterarItem(item);
                    return RedirectToAction("Início");
                }
                catch (Exception)
                {
                    resultado = "Não foi possível inserir esse item";
                }
            }

            return Json(new { Resultado = resultado, Mensagem = mensagem });
        }

        // Carrega a exibição de remover um item
        public ActionResult RemoverItem(int id)
        {
            if (!ModelState.IsValid)
            {
                resultado = "AVISO";
                mensagem = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            }
            else
            {
                try
                {
                    Models.ItemModel model = business.FiltrarPorId(id);
                    return View(model);
                }
                catch (Exception)
                {
                    resultado = "Não foi possível acessar essa opção";
                }
            }

            return Json(new { Resultado = resultado, Mensagem = mensagem });
        }

        // Executa a ação de remover quando o botão é clicado
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoverItem(int id, Models.ItemModel item)
        {         
           try
           {
               business.RemoverItem(id);
               return RedirectToAction("Início");
           }
           catch (Exception)
           {
              resultado = "Não foi possível remover esse item";
              return Json(new { Resultado = resultado, Mensagem = mensagem });
           }
        }        
    }
}